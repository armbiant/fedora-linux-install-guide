:experimental:
include::{partialsdir}/entities.adoc[]

[[chap-pxe-server-setup]]
= Setting Up an Installation Server

[NOTE]
====

This appendix is intended for users with previous Linux experience. If you are a new user, you may want to install using minimal boot media or the distribution DVD instead.

====

[[pxe-overview]]
== PXE Installation Overview

Preboot Execution Environment, or PXE, is a techonology that allows computers to boot directly from resources provided over the network. Installing Fedora over the network means you don't have to create media, and you can install to multiple computers or virtual machine simultaneously. The process involves a number of components and features working together to provide the resources required.

.PXE-capable computer
Most modern computers have the capability to network boot. Typically, a function key pressed during boot will bring up a boot selection menu. In environments designed for unattended administration, systems will often be configured to first attempt booting from the network, then boot from local storage, and the installation server is configured to only offer the installation when required. Your computer's manual will provide specific instructions on setting boot priorities.

.DHCP Server
When a system requests an address during network booting, the DHCP server also provides the location of files to boot. A network should have only one DHCP server.

.TFTP Server
Because the pre-boot environment is very simple, files must be provided in a very simple way. Trivial File Transfer Protocol, or TFTP, provides the system with the bootloader required to continue the installation process.

.Bootloader
Because the job of booting an operating system is too complex for the pre-boot environment, a bootloader is used to load the kernel and related files. It also provides configuration information to the installer, and can offer a menu to select from different configurations.

.Kernel and Initramfs
The kernel is the core of any Linux operating system, and the initramfs provides the kernel with required tools and resources. These files are also provided by tftp.

.Package repository
A Fedora repository must be available for the installation. The example in this section uses the public Fedora mirrors as the repository source, but you can also use a repo on the local network provided by NFS, FTP, or HTTP. Repositories can be configured using the [option]#inst.repo=# boot option; see xref:advanced/Boot_Options.adoc#sect-boot-options-sources[Specifying the Installation Source] for details.

//A link to mirrormanager and some instructions to other guides too.  All the elaboration on installation methods might be going to far, but we can ref. --Pete

[[pxe-dhcpd]]
== DHCP Server Configuration

//Needs adminition about static IP, reference out to Networking Guide. Example assumes 192.168.1.2 for server.

.Installing and configuring dhcpd
. Install the dhcp server package.
+
[subs="macros"]
----
# dnf install dhcp-server
----

. Create a simple configuration for the dhcp server at `/etc/dhcp/dhcpd.conf`
+
[subs="quotes"]
----
subnet 192.168.1.0 netmask 255.255.255.0 {
authoritative;
default-lease-time 600;
max-lease-time 7200;
ddns-update-style none;

option domain-name-servers 192.168.1.1;
option routers 192.168.1.1;

}
----

. Test your configuration and address any problems you discover.
+
[subs="quotes, macros"]
----

[command]#systemctl start dhcpd#
[command]#systemctl enable dhcpd#
[command]#journalctl --unit dhcpd --since -2m --follow#
----

. Add entries to point clients to their bootloader and the server that provides it to your subnet configuration in `/etc/dhcp/dhcpd.conf`. Because DHCP clients provide the server with identifying information along with their address request, BIOS clients and UEFI clients can each be directed to the correct bootloader. Using latest processor architecture option codes, which may be found on the https://www.iana.org/assignments/dhcpv6-parameters/dhcpv6-parameters.xhtml#processor-architecture[IANA DHCPv6] registration page, allows multiple architectures to share a single DHCP server.
+
----
# refer to RFC4578 & IANA DHCPv6 for possible arch option values
option arch code 93 = unsigned integer 16;

subnet 192.168.1.0 netmask 255.255.255.0 {
if option arch = 00:07 {
# x64 UEFI
filename "uefi/shimx64.efi";
next-server 192.168.1.2;
} else if option arch = 00:0b {
# aarch64 UEFI
filename "uefi/shimaa64.efi";
server-name "192.168.1.2";
} else {
filename "pxelinux.0";
next-server 192.168.1.2;
}


...
----

. Restart the dhcp service to check the configuration and make changes as needed.
+
[subs="quotes, macros"]
----

[command]#systemctl restart dhcpd#
[command]#journalctl --unit dhcpd --since -2m --follow#

----

[[pxe-tftpd]]
== Installing the tftp server

.Installing the tftp server
. Install the tftp server package.
+
----
# dnf install tftp-server
----

. Start and enable the `tftp socket`. `systemd` will automatically start the `tftpd` service when required.
+
[subs="quotes, macros"]
----
# [command]#systemctl start tftp.socket#
# [command]#systemctl enable tftp.socket#
----

[[pxe-bootloader]]
== Providing and configuring bootloaders for PXE clients

.Getting the bootloader files
. Get the [package]*syslinux* bootloader for BIOS clients.
+
.. Install the [package]*syslinux* package.
+
----
# dnf install syslinux
----
+
.. Create a directory for the bootloader files, and make them available there.
+
[subs="quotes, macros"]
----
# [command]#mkdir -p `/var/lib/tftpboot/pxelinux.cfg`pass:attributes[{blank}]#
# [command]#cp `/usr/share/syslinux/{pxelinux.0,menu.c32,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32}` `/var/lib/tftpboot/`pass:attributes[{blank}]#
----

. Get the bootloader files for UEFI systems
+
.. Install the [package]*shim-x64* and [package]*grub2-efi-x64* packages. If your server is a BIOS system, you *must install the packages to a temporary install root*. Installing them directly on a BIOS machine will attempt to configure the system for UEFI booting and cause problems.
+
[subs="attributes"]
----
# dnf install shim-x64 grub2-efi-x64 --installroot=/tmp/fedora --releasever {PRODVER}
----
+
.. Create a directory for the bootloader files, and make them available there.
+
[subs="quotes, macros"]
----
# [command]#mkdir -p `/var/lib/tftpboot/uefi`pass:attributes[{blank}]#
# [command]#cp `/tmp/fedora/boot/efi/EFI/fedora/{shimx64.efi,grubx64.efi}` `/var/lib/tftpboot/uefi/`pass:attributes[{blank}]#
----

.Configuring client bootloaders
. Create a boot menu for BIOS clients at `/var/lib/tftpboot/pxelinux.cfg/default`.
+
//needs adminition about kickstarts here somewhere, and testing of pulling .ks out of cgit
+
[subs="attributes"]
----
default vesamenu.c32
prompt 1
timeout 600

label local
menu label Boot from ^local drive
menu default
localboot 0xffff

label linux
menu label ^Install Fedora {PRODVER} 64-bit
kernel f{PRODVER}/vmlinuz
append initrd=f{PRODVER}/initrd.img inst.stage2=https://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/ ip=dhcp

label server
menu label ^Install Fedora {PRODVER} ( Minimal Image )
kernel f{PRODVER}/vmlinuz
append initrd=f{PRODVER}/initrd.img inst.stage2=https://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/ ip=dhcp ks=https://example.com/fedora/kickstarts/minimal.ks
----

. Create a boot menu for UEFI clients at `/var/lib/tftpboot/uefi/grub.cfg`.
+
[subs="attributes"]
----
function load_video {
	insmod efi_gop
	insmod efi_uga
	insmod video_bochs
	insmod video_cirrus
	insmod all_video
}

load_video
set gfxpayload=keep
insmod gzio

menuentry 'Exit this grub' {
        exit
}

menuentry 'Install {PRODUCT} 64-bit'  --class fedora --class gnu-linux --class gnu --class os {
	linux $fw_path/f{PRODVER}/vmlinuz ip=dhcp inst.repo=https://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/
	initrd $fw_path/f{PRODVER}/initrd.img
}

menuentry 'Install Fedora {PRODVER} Server'  --class fedora --class gnu-linux --class gnu --class os {
	kernel f{PRODVER}/vmlinuz
	append initrd=f{PRODVER}/initrd.img inst.repo=https://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/ ip=dhcp ks=https://git.fedorahosted.org/cgit/spin-kickstarts.git/plain/fedora-install-server.ks?h=f21
}

----

[[pxe-kernel]]
== Getting the kernel and initrd

.Downloading the kernel and initrd
. Create a directory for the files.
+
[subs="quotes, macros, attributes"]
----
# [command]#mkdir -p `/var/lib/tftpboot/f{PRODVER}`pass:attributes[{blank}]#
----

. Download the kernel.
+
[subs="attributes"]
----
# wget https://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/images/pxeboot/vmlinuz -O /var/lib/tftpboot/f{PRODVER}/vmlinuz
----

. Download the initrd
+
[subs="attributes"]
----
# wget https://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/images/pxeboot/initrd.img -O /var/lib/tftpboot/f{PRODVER}/initrd.img
----

[[http-overview]]
== HTTP Installation Overview

HTTP/HTTPS boot is a technology that allows computers to boot directly from resources provided over the network. When used in conjunction with HTTPS the authenticity of the server is validated, and the use of HTTP offers a more reliable transport mechanism than PXE's TFTP. Installing Fedora this way avoids creating install media and allows multiple computers to install simultaneously. Many current UEFI implementations, including the EDK2 based firmware shipping with fedora's virtualization solutions, can directly boot from HTTP sources. A UEFI boot entry may be manually added that specifies a HTTP source via firmware menus. Alternatively, a DHCP server may automatically provide the required HTTP path.

.Enrolling Certificatesll
While many machines are capable of HTTPS boot as well as HTTP, they will frequently need to have a certificate authority (CA) enrolled first. The CA is used to validate the certificates presented by the HTTPS server. This may be accomplished by enrolling the appropriate files from the fedora provided ca-certificates for public mirrors or the local HTTPS boot server's certificate.


[[https-dhcpd]]
== DHCP Server Configuration for HTTP
The installation and configuration of a DHCP server for HTTP boot is identical to its configuration for <<pxe-dhcpd,PXE>> except that we need to amend the option arch conditions for HTTP clients
[subs="attributes"]
----
....

if option arch = 00:07 {
# x64 UEFI
filename "uefi/shim64.efi";
next-server 192.168.1.2;
} else if option arch = 00:0b {
# aarch64 UEFI
filename "uefi/shimaa64.efi";
server-name "192.168.1.2";
} else if option arch = 00:13 {
# aarch64 UEFI HTTP
option vendor-class-identifier "HTTPClient";
filename "http://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/aarch64/os/images/boot.iso";
} else if option arch = 00:10 {
# x64 UEFI HTTP
option vendor-class-identifier "HTTPClient";
filename "http://download.fedoraproject.org/pub/fedora/linux/releases/{PRODVER}/Server/x86_64/os/images/boot.iso";
} else {
filename "pxelinux.0";
next-server 192.168.1.2;
}

...
----

[[pxe-repositories]]
== Providing repositories

The examples in this section use the public Fedora mirrors as the package source. For faster installations, installing to many systems, or more isolated environments, you may wish to maintain a local repository.

Fedora Infrastructure maintains instructions for configuring a local mirror at link:++https://fedoraproject.org/wiki/Infrastructure/Mirroring++[]. The preferred method for providing repositories is via HTTP, and you can refer to the [citetitle]_xref:fedora:system-administrators-guide:index.adoc[{PRODUCT} System Administrator's Guide]_ to configure `httpd`.

[[sect-install-server-cobbler]]
== Advanced network installations with Cobbler

For more complex environments, {PRODUCT} offers the [package]*cobbler* installation server. Tasks like managing kickstart configurations, coordinating repositories, maintaining dns records, dhcp servers, and even puppet manifests are effectively automated by [package]*cobbler*.

While levaraging all of the features provided by cobbler can be relatively simple, the full functionality of this powerful tool is too broad to be documented in this guide. The cobbler community provides documentation at link:++https://cobbler.github.io/++[] to accompany the packages in the Fedora repository.

Alternatively, you may also be interested in **Foreman**. You can find official documentation as well as downloads on the project website at link:++https://www.theforeman.org/++[].
