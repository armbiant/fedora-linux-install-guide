
:experimental:

[[sect-installation-gui-network-configuration-advanced]]
==== Editing Network Interface Configuration

This section only details the most important settings for a typical wired connection used during installation. Many of the available options do not have to be changed in most installation scenarios and are not carried over to the installed system. Configuration of other types of networks is broadly similar, although the specific configuration parameters may be different. To learn more about network configuration after installation, see the [citetitle]_{PRODUCT} Networking{nbsp}Guide_, available at link:++https://docs.fedoraproject.org/++[].

To configure a network connection manually, select that connection in the list on the left side of the screen, and click the `Configure` button. A dialog will appear that allows you to configure the selected connection. The configuration options presented depends on the connection type - the available options will be slightly different depending on whether it is a physical interface (wired or wireless network interface controller) or a virtual interface (Bond, Team or Vlan) which you previously configured in xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-network-configuration-virtual-interface[Adding a Virtual Network Interface].. A full description of all configuration settings for all connection types is beyond the scope of this document; see the [citetitle]_Networking Guide_ for details.

The most common and useful options in the configuration dialog are:

Enable or disable the connection by default::  In the `General` tab of the configuration dialog, you can select or unselect the `Automatically connect to this network when it is available` check box to allow or disallow this connection to connect by default. When enabled on a wired connection, this means the system will typically connect during startup (unless you unplug the network cable); on a wireless connection, it means that the interface will attempt to connect to any known wireless networks in range.
+
Additionally, you can allow or disallow all users on the system from connecting to this network using the `All users may connect to this network` option. If you disable this option, only `root` will be able to connect to this network.
+
[NOTE]
====

It is not possible to only allow a specific user other than `root` to use this interface, because no other users are created at this point during the installation. If you need a connection for a different user, you must configure it after the installation.

====

Set up static IPv4 or IPv6 settings::  By default, both `IPv4` and `IPv6` are set to automatic configuration depending on current network settings. This means that addresses such as the local IP address, DNS address, and other settings will be detected automatically each time the interface connects to a network. In many cases, this is sufficient, but you can also provide static configuration in the `IPv4 Settings` and `IPv6 Settings`, respectively.
+
To set static network configuration, navigate to one of the settings tabs and select a method other than `Automatic` (for example, `Manual`) from the `Method` drop-down menu. This will enable the `Addresses` field below.
+
[NOTE]
====

In the `IPv6 Settings` tab, you can also set the method to `Ignore` to disable `IPv6` on this interface.

====
+
Then, click `Add` on the right side and add a set of settings: `Address`, `Netmask` (for `IPv4`), `Prefix` (for `IPv6`), and `Gateway`.
+
The `DNS servers` field accepts one or more IP addresses of DNS servers - for example, `10.0.0.1,10.0.0.8`.
+
The final option in both tabs is `Require IPvpass:attributes[{blank}]_X_ addressing for this connection to complete`. Select this option in the `IPv4` tab to only allow this connection if `IPv4` was successful; the same principle applies to this setting in the `IPv6` tab. If this option remains disabled for both `IPv4` and `IPv6`, the interface will be able to connect if configuration succeeds on either IP protocol.

Configure routes::  In the `IPv4 Settings` and `IPv4 Settings` tabs, click the `Routes` button in the bottom right corner to configure routing settings for a specific IP protocol on an interface. A new dialog will open, allowing you to `Add` a specific route.
+
If you confire at least one static route, you can disallow all routes not specifically configured here by enabling the `Ignore automatically obtained routes`.
+
Select `Use this connection only for resources on its network` to prevent this connection from becoming the default route. This option can be selected even if you did not configure any static routes. Enabling this option means that this route will only be used when necessary to access certain resources, such as intranet pages which require a local or VPN connection. Another (default) route will be used for publicly available resources if possible. Note that unlike the additional routes configured in this dialog, this setting will be transferred to the installed system. Also note that this option is only useful when more than one interface is configured.
+
When you finish configuring the interface's routing settings, click `OK` to return to the configuration dialog.

Once you finish configuring the interface, click `Save` in the configuration window's bottom right corner to save your settings and return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-network-configuration[Network & Hostname].
